﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("=================Console类=================");
            //Console.WriteLine("请输入一个字符：");
            //int num = Console.Read();
            //Console.WriteLine(num);
            //char c = (char)num;
            //Console.WriteLine(c);

            //Console.WriteLine("请按下一个键：");
            //ConsoleKeyInfo cki = Console.ReadKey();
            //Console.WriteLine();
            //Console.WriteLine("你按下的键是："+cki.KeyChar);

            //Console.WriteLine("=================Math类=================");
            //Console.WriteLine("Math.Ceiling(3.1) = " + Math.Ceiling(3.1));//4
            //Console.WriteLine("Math.Floor(3.1) = " + Math.Floor(3.9));//3

            //int result;
            //int num = Math.DivRem(5,3,out result);//8是被除数，3是除数，那么是 8/3 还是3/8 ？ 是8/3
            //Console.WriteLine("5/3的商是{0}，余数是{1}",num,result);

            //保留N位小数
            //double dou = 1.23456;
            //Console.WriteLine("Math.Round(dou, 3)  = " + Math.Round(dou, 3));//保留3位小数，四舍五入
            //Console.WriteLine("dou.ToString(\"#0.000\") = "+dou.ToString("#0.000"));//保留3位小数，四舍五入
            //Console.WriteLine("dou.ToString(\"f3\") = "+dou.ToString("f3"));//保留3位小数，四舍五入
            //Console.WriteLine(Math.Round(1.93456));

            //不四舍五入的方法1
            //string douString = dou.ToString();//1.23456  要保留3位小数，那就是要截取到1.234，如果我知道了小数点的位置，可以知道要截取的长度了码？
            //保留3位小数，用截取的办法，那我们肯定是从0开始截取吧。
            //那问题来了，那要截取多长呢？就是截取的长度是多少呢？

            //如果是123.456789 截取的长度是7
            //如果是12.3456789 截取的长度6
            //规律找出来了，小数点的索引位置 加上 要保留的小数位+1
            //Console.WriteLine(douString.Substring(0, douString.IndexOf(".") + 4));

            //不四舍五入的方法2
            //int intNum = (int)(dou * 1000);//1.23456  1234.56  1234
            //Console.WriteLine(intNum/1000.0);

            //不四舍五入的方法3
            //Console.WriteLine(Math.Floor(dou * 1000)/1000.0);
            //Console.WriteLine(Math.Ceiling(dou * 1000)/1000.0);


            //Console.WriteLine("=================Random类=================");
            //Random ran = new Random();
            //for (int i = 0; i < 30; i++)
            //{
            //    //Console.WriteLine(ran.Next(3));//ran.Next(N)，产生 0-N 的随机整数，不包含N
            //    //Console.WriteLine(ran.Next(3, 5));//ran.Next(N,M)，产生 N-M 的随机整数，不包含M
            //    Console.WriteLine(ran.NextDouble());//ran.NextDouble()，产生大于0小于1的浮点数
            //}

            //生成0-3之间的随机小数
            //先随机0-3之间的整数，再加上0-1之间的随机小数
            //ran.NextDouble();
            //生成 min-max之间的小数。
            //Math.Round(random.NextDouble() * (max - min) + min, Len);

            Console.WriteLine("=================DateTime类=================");
            DateTime now = DateTime.Now;
            Console.WriteLine(now.ToString());
            Console.WriteLine(now.Year);
            Console.WriteLine(now.Month);
            Console.WriteLine(now.DayOfWeek);
            Console.WriteLine((int)now.DayOfWeek);

            //字符串转成DateTime
            string datetime = "2021-06-01";
            DateTime dt = Convert.ToDateTime(datetime);
            Console.WriteLine(dt);
            Console.WriteLine(dt.Day);

            //时间计算
            Console.WriteLine(now.AddDays(-4));

            //如何通过今天，获得本周周天的时间。
            //分析，周五，5，周天 0 
            string datetime1 = "2021-05-23";
            Console.WriteLine(Convert.ToDateTime(datetime1));
            Console.WriteLine((int)Convert.ToDateTime(datetime1).DayOfWeek);

            Console.WriteLine(now.AddDays(-(int)now.DayOfWeek));//获取本周周天
            Console.WriteLine(now.AddDays(6-(int)now.DayOfWeek));//获取本周周六
            //生成3-5之间的随机小数






            Console.ReadKey();
        }

        static void Test()
        {
            string str = "类与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，" +
                "即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。" +
                "在使用定义好的类类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，" +
                "也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中" +
                "仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，类让类或接口中的成员表现出不同的作用。";

            //可以把字符串看作是一个字符数组

            //1、使用循环遍历的方法来实现。
            //ForString(str);

            //2、使用Replace方法来实现。
            //ReplaceString(str);

            //3、使用Split()方法来实现。
            SplitString(str);

            //string str = "a,b,,c,";
            //str.Split(seprator1,StringSplitOptions.RemoveEmptyEntries);
            StringBuilder sb = new StringBuilder();
            Console.WriteLine("请输入你的姓名：");
            sb.Append(Console.ReadLine());
            Console.WriteLine("请输入你的年龄：");
            sb.Append(Console.ReadLine());
            Console.WriteLine("请输入你的家庭住址：");
            sb.Append(Console.ReadLine());
            Console.WriteLine("请输入你的兴趣爱好：");
            sb.Append("，兴趣爱好：" + Console.ReadLine());

            Console.WriteLine("你输入的信息是：{0}", sb.ToString());
        }

        private static void SplitString(string str)//用Split方法
        {
            char[] seprator1 = { '类' };
            char[] seprator2 = { '码' };
            int num1 = str.Split(seprator1).Length - 1;
            int num2 = str.Split(seprator2).Length - 1;
            Console.WriteLine("类有{0}个，码有{1}个", num1, num2);
        }

        static void ReplaceString(string str)//用Replace方法
        {
            //str
            int num1 = str.Length - str.Replace("类", "").Length;
            int num2 = str.Length - str.Replace("码", "").Length;
            Console.WriteLine("类有{0}个，码有{1}个", num1, num2);
        }

        static void ForString(string str)//用遍历循环的方法
        {
            //获取字符串长度
            //str.Length;

            //获取字符串中的某个字符
            char c = str[25];
            int num1 = 0;
            int num2 = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '类')
                {
                    num1++;
                }
                else if (str[i] == '码')
                {
                    num2++;
                }
            }
            Console.WriteLine("类有{0}个，码有{1}个", num1, num2);
        }
    }
}
