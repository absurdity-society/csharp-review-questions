﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 字符串的方法
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            int num1 = 0;
            int num2 = 0;
            foreach (char item in str)
            {
                if (item=='类')
                {
                    num1++;
                }
                if (item=='码')
                {
                    num2++;
                }
            }

            string str1 = "C# (英 文名为 CSharp) 是 微  软开发的一种 面向对 象的 编程 语言。C# 语言具备了面向对象 语言 的特 征，  即封装、继承、多态，并且添加了  事件和委托，增强了 编程的灵 活性。C# 语 言是  一种安全的、稳定的、简 单 的、面向对象的编程 语言 ，其语 法与 C++ 类似，但在编程过  程中要比 C++ 简单；它不仅去掉了 C++ 和 Java 语  言中的一些复杂特性，还提  供了可视化 工具，能够高效地 编写程序。C# 是运行  在.NE  T平台之上的  编程 语言。";
            string str2 = str1.Replace(" ","");
            int num3 = str1.Length - str.Length;
            int num4 = str1.Length;
            Console.WriteLine(str2);
            Console.WriteLine("空格的个数是{0},原来{1}",num3,num4);
        }
        public static void T1(string str) 
        {
            int num1 = str.Length- str.Replace("类", "").Length;
            int num2 = str.Length - str.Replace("码", "").Length;
            Console.WriteLine("类的个数是{num1}，码的个数是{num2}");

        }
        public static void T2(string str) 
        {
            int num1=str.Split('类').Length-1;
            int num2 = str.Split('码').Length - 1;
            Console.WriteLine("类的个数是{num1}，码的个数是{num2}");
        }
    }
}
