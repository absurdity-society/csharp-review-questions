﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 类和属性复习
{
    class Students
    {
        private string _id;
        private string _name;
        private string _password;

        public Students(string id, string name, string password)
        {
            Id = id;
            Name = name;
            Password = password;
        }
        public Students() { }
        public string Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Password { get => _password; set => _password = value; }

        public void Say() 
        {
            Console.WriteLine("您的用户名是{0},您的ID是{1}，您的密码是{2}",this.Name,this.Id,this.Password);
        }
    }
}
