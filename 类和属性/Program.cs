﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 类和属性复习
{
    class Program
    {
        static void Main(string[] args)
        {
            Users st1 = new Users();
            st1.Id = 1;
            st1.Name = "aaa";
            st1.Sex = "man";
            st1.Age = 12;
            st1.Major = "教练";
            st1.Say();

            Students user1 = new Students();
            user1.Id = "123456";
            user1.Name = "bbb";
            user1.Password = "123456";
            user1.Say();

        }
    }
}
