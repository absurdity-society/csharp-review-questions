﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 类和属性复习
{
    class Users
    {
        private int _id;
        private string _name;
        private string _sex;
        private string _major;
        private int _age;

        public Users(int id, string name, string sex, string major, int age)
        {
            Id = id;
            Name = name;
            Sex = sex;
            Major = major;
            Age = age;
        }
        public Users() { }
        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public string Sex { get => _sex; set => _sex = value; }
        public string Major { get => _major; set => _major = value; }
        public int Age
        {
            get { return this._age; }
            set 
            {
                if (value>0|| value < 128)
                {
                    Age = value;
                }
                else
                {
                    Age = 0;
                }
            }
        }

        public void Say()
        {
            Console.WriteLine("学生ID：{0}，学生姓名：{1}，学生性别：{2}，学生年龄：{3}，学生专业：{4}",this.Id, this.Name, this.Sex, this.Age, this.Major);
        }
    }
}
