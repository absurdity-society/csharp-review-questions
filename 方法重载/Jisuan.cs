﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 方法重载
{
    class Jisuan
    {
        private int _chang;
        private int _kuan;
        private double _r;

        public Jisuan(int chang, int kuan, double r)
        {
            Chang = chang;
            Kuan = kuan;
            R = r;
        }
        public Jisuan() { }
        public int Chang { get => _chang; set => _chang = value; }
        public int Kuan { get => _kuan; set => _kuan = value; }
        public double R { get => _r; set => _r = value; }

        public void Mianji(int Chang, int Kuan)
        {
            int sum1 = Chang * Kuan;
            Console.WriteLine("这个矩形的面积为" + sum1);
        }
        public void Mianji(double R)
        {
            double sum2 = R * R * 3.14;
            Console.WriteLine("这个圆的面积为" + sum2);
        }
    }
}
