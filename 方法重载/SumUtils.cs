﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 方法重载
{
    class SumUtils
    {
        private int _int1;
        private int _int2;
        private int _int3;
        private double _double1;
        private double _double2;
        private string _str1;
        private string _str2;

        public SumUtils(int int1, int int2, int int3, double double1, double double2, string str1, string str2)
        {
            Int1 = int1;
            Int2 = int2;
            Int3 = int3;
            Double1 = double1;
            Double2 = double2;
            Str1 = str1;
            Str2 = str2;
        }
        public SumUtils() { }
        public int Int1 { get => _int1; set => _int1 = value; }
        public int Int2 { get => _int2; set => _int2 = value; }
        public int Int3 { get => _int3; set => _int3 = value; }
        public double Double1 { get => _double1; set => _double1 = value; }
        public double Double2 { get => _double2; set => _double2 = value; }
        public string Str1 { get => _str1; set => _str1 = value; }
        public string Str2 { get => _str2; set => _str2 = value; }

        public void Sum(int Int1, int Int2)//计算两个整数和
        {
            int sum = Int1 + Int2;
            Console.WriteLine("两个整数和为"+sum);
        }
        public void Sum(double Double1,double Double2) 
        {
            double sum = Double1 + Double2;
            Console.WriteLine("两个小数和为"+sum);
        }
    }
}
