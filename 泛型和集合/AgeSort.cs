﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 泛型和数组
{
    class AgeSort : IComparer<Students>
    {
        public int Compare(Students x, Students y)
        {
            return x.Age.CompareTo(y.Age);
        }
    }
}
