﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 泛型和数组
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Students> students = new List<Students>();
            Students s2 = new Students(1,"猛男叶真",12);
            students.Add(s2);
            Boolean falg1 = true;
            while (falg1==true)
            {
                Console.WriteLine("请选择：1.添加 2.查询 3.删除 4.退出");
                int key = int.Parse(Console.ReadLine());
                switch (key)
                {
                    case 1 : Add(students);
                        break;
                    case 2:
                        Cha(students);
                        break;
                    case 3:
                        Shan(students);
                        break;
                    case 4:falg1 = false;
                        break;
                    default:
                        Console.WriteLine("您的输入有误");
                        break;
                }
            }

        }
        public static void Add(List<Students> students)
        {
            Console.WriteLine("请输入您需要添加的学号");
            int id = int.Parse(Console.ReadLine());
            int index = -1;
            for (int i = 0; i < students.Count; i++)
            {
                if (id==students[i].Id)
                {
                    index = 1;
                }
            }
            if (index==1)
            {
                Console.WriteLine("对不起，您的输入有误");
            }
            else
            {
                Console.WriteLine("请输入您需要添加的姓名");
                string name = Console.ReadLine();
                Console.WriteLine("请输入你需要添加的年龄");
                int age = int.Parse(Console.ReadLine());
                Students s1 = new Students(id,name,age);
                students.Add(s1);
            }
        }
        public static void Cha(List<Students> students) 
        {
            Boolean falg = true;
            while (falg==true)
            {
                Console.WriteLine("请选择查询方式：1、查询所有（按学号排序）2、查询所有（按姓名排序），2、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
                int key2 = int.Parse(Console.ReadLine());
                IComparer<Students> comparer;
                switch (key2)
                {
                    case 1: comparer =new IdSort();
                        students.Sort(comparer);
                        foreach (Students item in students)
                        {
                            Console.WriteLine(item);
                        }
                        break;
                    case 2:
                        comparer = new NameSort();
                        students.Sort(comparer);
                        foreach (Students item in students)
                        {
                            Console.WriteLine(item);
                        }
                        break;
                    case 3:
                        comparer = new AgeSort();
                        students.Sort(comparer);
                        foreach (Students item in students)
                        {
                            Console.WriteLine(item);
                        }
                        break;
                    case 4:
                        XueCha(students);
                        break;
                    case 5:
                        falg = false;
                        break;                       
                    default:
                        Console.WriteLine("您的输入有误，请重新输入");
                        falg = true;
                        break;
                }

                
            }

        }
        public static void XueCha(List<Students> students)
        {
            Console.WriteLine("请输入需要查询的学号");
            int key3 = int.Parse(Console.ReadLine());
            int index1 = -1;
            for (int i = 0; i < students.Count; i++)
            {
                if (students[i].Id==key3)
                {
                    index1 = key3;
                }
                else
                {
                    
                }
            }
            if (index1 == -1)
            {
                Console.WriteLine("您的输入有误，请重新输入");
            }
            else
            {
                Console.WriteLine(students[index1]);
            }
        }
        public static void Shan(List<Students> students) 
        {
            Console.WriteLine("请输入您需要删除的学号");
            int key4 = int.Parse(Console.ReadLine());
            int index = -1;
            for (int i = 0; i < students.Count; i++)
            {
                if (key4==students[i].Id)
                {
                    index = key4;
                }
                else
                {
                    
                }
            }
            if (index==-1)
            {
                Console.WriteLine("对不起，您的输入有误");
            }
            else
            {
                students.Remove(students[index]);
                Console.WriteLine("删除成功");
            }
        }

    }
}
