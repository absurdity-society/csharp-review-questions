﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 泛型和数组
{
    class Students
    {
        private int _id;
        private string _name;
        private int _age;

        public Students(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }

        public int Id { get => _id; set => _id = value; }
        public string Name { get => _name; set => _name = value; }
        public int Age { get => _age; set => _age = value; }

        public override string ToString()
        {
            return $"学号是：{this.Id}，姓名是{this.Name}，年龄是{this.Age}";
        }
    }
}
