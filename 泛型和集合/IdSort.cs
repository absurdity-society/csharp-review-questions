﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 泛型和数组
{
    class IdSort : IComparer<Students>
    {
        public int Compare(Students x, Students y)
        {
            return x.Id.CompareTo(y.Id);
        }
    }
}
