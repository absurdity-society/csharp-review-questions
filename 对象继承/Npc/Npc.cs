﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    abstract class Npc
    {
        private string _name;
        private string _type;

        public abstract void Function();

        public string Name { get => _name; set => _name = value; }
        public string Type { get => _type; set => _type = value; }
    }
}
