﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Craftsman : Npc
    {
        private string _work;

        public Craftsman(string work, string name, string type)
        {
            this.Work = work;
            this.Name = name;
            this.Type = type;
        }

        public string Work { get => _work; set => _work = value; }

        public override void Function()
        {
            Console.WriteLine("你好，旅行者，我是{0}，有什么{1}上的事能帮到您？",this.Name,this.Work);
        }
    }
}
