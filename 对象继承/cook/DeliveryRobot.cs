﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class DeliveryRobot : Robot
    {
        private double _time;

        public double Time { get => _time; set => _time = value; }
        public DeliveryRobot()
        {
            this.Name = "送的快";
            this.Type = "服务机器人";
            this.Time = 8.0;
        }

        public override void Working()
        {
            Console.WriteLine("你好，我是"+this.Type+this.Name);
            Console.WriteLine("这是您点的菜。");
        }
    }
}
